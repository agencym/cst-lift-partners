$(document).ready(function() {

  // Study-2 Insight #2 Hover triggers background image change
	//$('.btn1').on('mouseenter mouseleave', function(e) {
  //  var image = e.type === 'mouseenter' ? 'new' : 'original';
  //  $(this).parent().css('background-image', 'url(../images/' + image + '-study-2-3hover.png)');
//});

	// Menu
	function toggleMenu() {
		$("#meat").toggleClass("reveal");
		$("#hamburger").toggleClass("close");
	}

	$("#hamburger").click(function() {
		toggleMenu();
		return false;
	});

	$("#meat a").click(function() {
		if($("#meat").hasClass("reveal")) {
			toggleMenu();
		}
	});


	// Set each case study earlier
	$(".case-study").each(function() {
		var thisId = $(this).attr("data-id");
		$(this).find(".study-intro").append('<p class="view-more"><a href="#study-info-' + thisId + '">View Case Study</a></p>');
		$(this).find(".study-info").hide().attr({"id":"study-info-" + thisId});
		$(this).find(".view-more a").click(function() {
			$(this).parents(".case-study").find(".study-info").slideToggle();
			$(this).fadeOut();
		});
		if ($(this).hasClass("nolink")) {
			$(this).find(".view-more").hide();
		}
	});


	// Set the height of sections/case studies to window height
	function setHeight() {
		if ($(window).width()> 768) {
			$("section").each(function() {
				if ($(this).hasClass("case-study")) {
					$(this).find(".study-intro, .info-inner").height($(window).height());
					if ($(window).width()>1020) {
						$(this).find(".study-intro, .info-inner").addClass("nopadding");
					} else {
						$(this).find(".study-intro, .info-inner").removeClass("nopadding");
					}
				} else {
					$(this).height($(window).height());
					if ($(window).width()>1020) {
						$(this).addClass("nopadding");
					} else {
						$(this).removeClass("nopadding");
					}
				}
			});
		}
	}

	$("input, textarea").each(function() {
		$(this).focus(function() {
			$(this).parents(".input-wrapper").addClass("focus");
			$(this).prev("label").find("sup").fadeOut();
		});
		$(this).blur(function() {
			$(this).parents(".input-wrapper").removeClass("focus");
		});
	});

	// Validate form
	$("button[type=submit]").click(function() {
		var fail = false;
		$(".msg").slideUp('fast');
		$("input, textarea").each(function() {
			if ($(this)[0].hasAttribute("required") && $(this).val()==='') {
				if ($(this).prev("label").find(".req").length>0) {
					$(this).prev("label").find(".req").fadeIn();
				} else {
					$(this).prev("label").append('<sup class="req"> * Required</sup>');

				}
				if (fail !== true) {
					fail = true;
				}
			}
		});
		if (fail === true) {
			$(".validate").slideDown();
		} else {
			$.post("contact.php", {
				name		: $("#name").val(),
				email		: $("#email").val(),
				company		: $("#company").val(),
				comments	: $("#comments").val()
			}, function(data) {
				switch (data) {
					case 'error':
						$(".tech").slideDown();
						break;
					case 'email':
						if ($("#email").prev("label").find(".invalid").length>0) {
							$("#email").prev("label").find(".invalid").fadeIn();
						} else {
							$("#email").prev("label").append('<sup class="invalid"> * Invalid</sup>');
						}
						$(".email").slideDown();
						break;
					case 'success':
						$(".success").slideDown();
						$("form")[0].reset();
						break;
					default:
						break;
				}
			})
		}

		return false;
	});

	function setMagic() {
		/* Scroll stuff */
		if ($(window).width()>768) {
			var fadein_tween = TweenMax.to('#intro .col-2', .375,{ opacity: 1, y:-50 });
			var fadein_tween2 = TweenMax.to('#intro-4 .col-2', .375,{ opacity: 1, y:-50 });
			var controller = new ScrollMagic.Controller();

			var hold_intro = new ScrollMagic.Scene({
			  duration: "175%",
			  offset: 0
			})
			.setPin("#intro")
			.setClassToggle("#intro .col-2","yes")
			.addTo(controller);

			var fadein_scene = new ScrollMagic.Scene({
			  duration: 200,
			  offset: 400,
			  reverse: true
			})
			.setTween(fadein_tween)
			.addTo(controller);

		// 	if (!$("html").hasClass("alt")) {
		// 	var fadeout_tween = TweenMax.to('#intro .col-1', .375,{ opacity: 0, y:-50 });
		// 	var fadeout_scene = new ScrollMagic.Scene({
		// 	  duration: 200,
		// 	  offset: 100,
		// 	  reverse: true
		// 	})
		// 	.setTween(fadeout_tween)
		// 	.addTo(controller);
		// 	}

			var hold_outro = new ScrollMagic.Scene({
			  duration: "175%",
			  triggerElement: "#intro-4",
			  triggerHook: 0
			})
			.setPin("#intro-4")
			.addTo(controller);

			var outro_fadein = new ScrollMagic.Scene({
			  duration: 200,
			  triggerElement: "#intro-4",
			  offset: 700
			})
			.setTween(fadein_tween2)
			.addTo(controller);

		// 	if (!$("html").hasClass("alt")) {
		// 	var fadeout_tween2 = TweenMax.to('#intro-4 .col-1', .375,{ opacity: 0, y:-50 });
		// 	var outro_fadeout = new ScrollMagic.Scene({
		// 	  duration: 200,
		// 	  triggerElement: "#intro-4",
		// 	  triggerHook: 0
		// 	})
		// 	.setTween(fadeout_tween2)
		// 	.addTo(controller);
		// 	}
		}
	}

	$(window).resize(function() {
		setHeight();
	});
// 	setMagic();
	setHeight();

	$(window).scroll(function() {
		if ($(window).scrollTop() > 0) {
			$("#nav").addClass("scroll");
		} else {
			$("#nav").removeClass("scroll");
		}
	});

	$("nav a, .request a, .view-more a").smoothScroll();

// Tooltip Section

  // Button Triggers
	$(".buttons").hover( function() {
		  var thisID = $(this).attr("id");
		  $("div.trigger-"+thisID).addClass("active");
		},function() {
			var thisID = $(this).attr("id");
		  $("div.trigger-"+thisID).removeClass("active");
		});
	$(".buttons").click( function() {
			return false
		});

	// First Button Hover
	$bgimg1 = $('#bgimg1');
	$('#btn-1').hover(
	    // when hovered
	    function() {
	        $bgimg1.css('opacity','0');
	    },
	    // when NOT hovered
	    function() {
	        $bgimg1.css('opacity','1');
	    }
	);

// get user-agent info, append to html tag
	var b = document.documentElement;
	b.setAttribute('data-useragent', navigator.userAgent);
	b.setAttribute('data-platform', navigator.platform );
	b.className += ((!!('ontouchstart' in window) || !!('onmsgesturechange' in window))?' touch':'');

});
